# Python程序开发课程设计报告：学生成绩测评系统

## 项目简介

本项目是《Python程序开发》课程设计的一部分，旨在通过Python编程实现一个学生成绩测评系统。该系统能够管理学生的成绩信息，包括学号、姓名、院系、三门课成绩、考试平均成绩、同学互评分、任课教师评分以及综合测评总分。系统还支持学生成绩信息的保存、读取、输入、输出、查找、删除和修改等功能。

## 功能描述

### 1. 需求分析

- **学生成绩信息管理**：通过类的知识实现学生成绩信息的存储，包括学号、姓名、院系、三门课成绩、考试平均成绩、同学互评分、任课教师评分、综合测评总分。其中，综合测评总分由考试平均成绩的70%、同学互评分的10%和任课教师评分的20%组成。
  
- **数据存取**：能够实现学生成绩信息的保存和读取，使用数据库对数据进行存取。

- **功能实现**：实现所有相关信息的输入、输出、查找、删除、修改等功能。

- **系统界面**：系统界面应至少实现控制台界面，并支持使用桌面窗体界面进行交互。

- **Excel文件操作**：通过`xlrd`和`xlwt`模块读取和写入Excel文件。

### 2. 功能设计与分析

- **数据库操作**：使用`PyMySQL`模块操作数据库对数据进行存取。首先安装`PyMySQL`模块：
  ```bash
  pip install PyMySQL
  ```
  使用时直接导入即可：
  ```python
  import pymysql
  ```

- **数据库表创建**：创建数据库`school`，并在其中创建数据表`student_sore`和`teacher_login`。可以使用`Navicat for MySQL`创建，也可以使用预处理语句创建表，若表不存在则创建，若存在则跳过。

## 使用说明

1. **安装依赖**：
   ```bash
   pip install -r requirements.txt
   ```

2. **数据库配置**：
   根据项目中的数据库配置文件，配置数据库连接信息。

3. **运行程序**：
   运行主程序文件，启动学生成绩测评系统。

## 贡献

欢迎任何形式的贡献，包括但不限于代码优化、功能扩展、文档完善等。请提交Pull Request或Issue进行讨论。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

希望通过本项目，能够帮助大家更好地理解和掌握Python编程以及数据库操作的相关知识。如果有任何问题或建议，欢迎随时联系。